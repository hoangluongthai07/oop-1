﻿Shape c1 = new Circle(3);
Shape c2 = new Circle(7);
Shape c3 = new Circle(10);

Shape r1 = new Rectangle(3, 2);
Shape r2 = new Rectangle(7, 3);
Shape r3 = new Rectangle(10, 5);

var arr = new[] { c1, c2, c3, r1, r2, r3 };
for (int i = 0; i < arr.Length; i++)
{
    // dù cho là đối tượng của Circle/Rectangle:
    // đều đối sử như là object của Shape nhờ kế thừa
    var shapeObj = arr[i];
    // mỗi object của Circle/Rectangle thì lại định nghĩa các tính riêng đây chính là đa hình
    // cùng 1 đối tượng cùng 1 hành vi nhưng cách thể hiện lại khác nhau(các tính chu vi và diện tích khác nhau)
    var area = shapeObj.GetArea();
    var peri = shapeObj.GetPerimeter();
    // xem object này là kiểu gì
    Console.WriteLine(shapeObj.GetType());
    Console.WriteLine(area);
    Console.WriteLine(peri);
}
