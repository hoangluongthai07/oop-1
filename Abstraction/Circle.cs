﻿// See https://aka.ms/new-console-template for more information
class Circle : Shape
{
    // đây chính là tính đóng gói: che giấu thông tin
    private readonly int radius;
    public Circle(int _radius) => radius = _radius;

    // đây chính là đa hình mỗi đối tượng của class khác nhau thì hành vi sẽ khác nhau
    public override double GetArea() => Math.PI * radius * radius;

    // đây chính là đa hình mỗi đối tượng của class khác nhau thì hành vi sẽ khác nhau
    public override double GetPerimeter() => 2 * Math.PI * radius;
}
