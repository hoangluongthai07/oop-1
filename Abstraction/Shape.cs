﻿
abstract class Shape
{
    // đây chính là biểu hiện của abstraction che dấu cách hoạt động
    public abstract double GetArea();
    // đây chính là biểu hiện của abstraction che dấu cách hoạt động
    public abstract double GetPerimeter();
}
